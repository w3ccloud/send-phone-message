package com.jimmy.phone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SendPhoneMessageApplication {

	public static void main(String[] args) {
		SpringApplication.run(SendPhoneMessageApplication.class, args);
	}

}
